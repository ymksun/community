package com.ymksun.community.util.string;

public class StringHelper {

	public static boolean isEmailValidate(String mail) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return mail.matches(regex);
	}
}
