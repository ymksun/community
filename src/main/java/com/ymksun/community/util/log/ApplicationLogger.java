package com.ymksun.community.util.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationLogger {

	protected Logger log;

	public ApplicationLogger(Class<?> clazz) {
		log = LogManager.getLogger(clazz);
	}

	// INFO

	public void info(String text) {
		log.info(() -> text);
	}

	public void info(String methodName, String text) {
		log.info(() -> format(methodName, text));
	}

	// DEBUG

	public void debug(String text) {
		log.debug(() -> text);
	}

	public void debug(String methodName, String text) {
		log.debug(() -> format(methodName, text));
	}

	// ERROR

	public void error(String text) {
		log.error(() -> text);
	}

	public void error(String methodName, String text) {
		log.error(() -> format(methodName, text));
	}

	private String format(String methodName, String text) {
		return "[" + methodName + "] " + text;
	}
}
