package com.ymksun.community.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class UserDto {

	@NonNull
	private long id;
	private String name;
	private String email;
	private String address;

}
