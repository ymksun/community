package com.ymksun.community.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class CommentDto {

	private long id;
	private String content;
	private long userId;
	private long postId;

}
