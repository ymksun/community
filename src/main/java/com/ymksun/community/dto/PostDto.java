package com.ymksun.community.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class PostDto {

	@NonNull
	private long id;
	private String content;
	private String privacy;
	private String status;
	private long userId;

}
