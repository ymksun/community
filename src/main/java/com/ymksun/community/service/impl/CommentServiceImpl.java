package com.ymksun.community.service.impl;

import java.util.List;

import com.ymksun.community.component.CommentMapper;
import com.ymksun.community.component.PostMapper;
import com.ymksun.community.component.UserMapper;
import com.ymksun.community.dto.PostDto;
import com.ymksun.community.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ymksun.community.enumeration.ActionType;
import com.ymksun.community.enumeration.EntityType;
import com.ymksun.community.exception.ActionFailedException;
import com.ymksun.community.exception.EntityNotFoundException;
import com.ymksun.community.model.Comment;
import com.ymksun.community.model.Post;
import com.ymksun.community.model.User;
import com.ymksun.community.dto.CommentDto;
import com.ymksun.community.repository.CommentRepository;
import com.ymksun.community.repository.PostRepository;
import com.ymksun.community.repository.UserRepository;
import com.ymksun.community.service.CommentService;
import com.ymksun.community.util.log.ApplicationLogger;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PostRepository postRepository;

	@Autowired
	UserMapper userMapper;

	@Autowired
	PostMapper postMapper;

	@Autowired
	CommentMapper commentMapper;

	ApplicationLogger log = new ApplicationLogger(CommentServiceImpl.class);

	@Override
	public List<CommentDto> getList(String filter) {
		List<Comment> commentList;
		if (filter != null && !filter.isEmpty() && !filter.equalsIgnoreCase("all")) {
			commentList = commentRepository.search(filter);
		} else {
			commentList = commentRepository.findAll();
		}

		return commentMapper.mapEntityToDtoList(commentList);
	}

	@Override
	public CommentDto getById(long id) {
		Comment comment = commentRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.COMMENT.getName(), "id", id)
				);
		return commentMapper.mapEntityToDto(comment);
	}

	@Override
	public CommentDto save(CommentDto commentDto) {
		if (commentDto == null || !isUserAndPostValid(commentDto)) {
			log.error("save", "Comment value is null or not valid.");
			throw new ActionFailedException(EntityType.COMMENT.getName(), ActionType.CREATE.getName());
		}

		Comment comment = commentRepository.save(commentMapper.mapDtoToEntity(commentDto));
		return commentMapper.mapEntityToDto(comment);
	}

	@Override
	public CommentDto update(long id, CommentDto commentDto) {
		if (commentDto == null || !isUserAndPostValid(commentDto)) {
			log.error("update", "Comment value is null or not valid.");
			throw new ActionFailedException(EntityType.COMMENT.getName(), ActionType.UPDATE.getName());
		}

		Comment comment = commentRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.COMMENT.getName(), "id", id)
				);

		comment.setContent(commentDto.getContent());
		comment.setUser(userMapper.mapDtoToEntity(new UserDto(commentDto.getUserId())));
		comment.setPost(postMapper.mapDtoToEntity(new PostDto(commentDto.getPostId())));

		Comment updatedComment = commentRepository.save(comment);
		return commentMapper.mapEntityToDto(updatedComment);
	}

	@Override
	public void delete(long id) {
		Comment comment = commentRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.COMMENT.getName(), "id", id)
				);
		commentRepository.delete(comment);
	}

	// TODO: refactor with exception.
	private boolean isUserAndPostValid(CommentDto commentDto) {
		User user = userRepository.findById(commentDto.getUserId()).orElseThrow(() -> new EntityNotFoundException(EntityType.COMMENT.getName(), "userId", commentDto.getUserId()));
		Post post = postRepository.findById(commentDto.getPostId()).orElseThrow(() -> new EntityNotFoundException(EntityType.COMMENT.getName(), "postId", commentDto.getPostId()));
		return user != null && post != null;
	}

}
