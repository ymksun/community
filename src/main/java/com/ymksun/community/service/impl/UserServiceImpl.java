package com.ymksun.community.service.impl;

import java.util.List;

import com.ymksun.community.component.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ymksun.community.enumeration.ActionType;
import com.ymksun.community.enumeration.EntityType;
import com.ymksun.community.exception.ActionFailedException;
import com.ymksun.community.exception.EntityNotFoundException;
import com.ymksun.community.model.User;
import com.ymksun.community.dto.UserDto;
import com.ymksun.community.repository.UserRepository;
import com.ymksun.community.service.UserService;
import com.ymksun.community.util.log.ApplicationLogger;
import com.ymksun.community.util.string.StringHelper;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserMapper userMapper;

	ApplicationLogger log = new ApplicationLogger(UserServiceImpl.class);

	@Override
	public List<UserDto> getList(String filter) {
		List<User> userList;
		if (filter != null && !filter.isEmpty()) {
			if (filter.equalsIgnoreCase("all")) {
				userList = userRepository.findAll();
			} else {
				userList = userRepository.search(filter);
			}
		} else {
			log.error("getList", "Filter value is not valid!");
			// return exception if filter value is not valid.
			throw new ActionFailedException(EntityType.USER.getName(), ActionType.SEARCH.getName());
		}

		if (userList == null) {
			log.error("getList", "User not found with filter value: " + filter);
			// return exception if user does not exist.
			throw new EntityNotFoundException(EntityType.USER.getName(), "filter", (filter != null ? filter : ""));
		}

		return userMapper.mapEntityToDtoList(userList);
	}

	@Override
	public UserDto getById(long id) {
		User user = userRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.USER.getName(), "id", id)
				);
		return userMapper.mapEntityToDto(user);
	}

	@Override
	public UserDto save(UserDto userDto) {
		validateDto(userDto, ActionType.CREATE.getName());

		User user = userRepository.save(userMapper.mapDtoToEntity(userDto));
		return userMapper.mapEntityToDto(user);
	}

	@Override
	public UserDto update(long id, UserDto userDto) {
		validateDto(userDto, ActionType.UPDATE.getName());

		User user = userRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.USER.getName(), "id", id)
				);
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setAddress(userDto.getAddress());

		User updatedUser = userRepository.save(user);
		return userMapper.mapEntityToDto(updatedUser);
	}

	@Override
	public void delete(long id) {
		User user = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(EntityType.USER.getName(), "id", id));

		userRepository.delete(user);
	}

	private void validateDto(UserDto userDto, String action) {
		if (userDto == null) {
			log.error("validateDto", "User value is null.");
			throw new ActionFailedException(EntityType.USER.getName(), action);
		}

		if (!StringHelper.isEmailValidate(userDto.getEmail())) {
			log.error("validateDto", "User Email is not valid!.");
			throw new ActionFailedException(EntityType.USER.getName(), action);
		}
	}
}
