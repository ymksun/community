package com.ymksun.community.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ymksun.community.component.PostMapper;
import com.ymksun.community.component.UserMapper;
import com.ymksun.community.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ymksun.community.enumeration.ActionType;
import com.ymksun.community.enumeration.EntityType;
import com.ymksun.community.enumeration.PostEnum;
import com.ymksun.community.exception.ActionFailedException;
import com.ymksun.community.exception.EntityNotFoundException;
import com.ymksun.community.model.Post;
import com.ymksun.community.model.User;
import com.ymksun.community.dto.PostDto;
import com.ymksun.community.repository.PostRepository;
import com.ymksun.community.repository.UserRepository;
import com.ymksun.community.service.PostService;
import com.ymksun.community.util.log.ApplicationLogger;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	PostRepository postRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserMapper userMapper;

	@Autowired
	PostMapper postMapper;

	ApplicationLogger log = new ApplicationLogger(PostServiceImpl.class);

	@Override
	public List<PostDto> getList(String filter) {
		List<Post> postList = new ArrayList<>();
		if (filter != null && !filter.isEmpty() && !filter.equalsIgnoreCase("all")) {
			postList = postRepository.search(filter);
		} else {
			postList = postRepository.findAll();
		}

		return postMapper.mapEntityToDtoList(postList);
	}

	@Override
	public PostDto getById(long id) {
		Post post = postRepository
				.findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.POST.getName(), "id", id)
				);
		return postMapper.mapEntityToDto(post);
	}

	@Override
	public PostDto save(PostDto postDto) {
		if (postDto == null || !isUserValid(postDto)) {
			log.error("save", "Post value is null or not valid.");
			throw new ActionFailedException(EntityType.POST.getName(), ActionType.CREATE.getName());
		}

		validateDto(postDto, ActionType.CREATE.getName());

		Post post = postRepository.save(postMapper.mapDtoToEntity(postDto));
		return postMapper.mapEntityToDto(post);
	}

	@Override
	public PostDto update(long id, PostDto postDto) {
		if (postDto == null || !isUserValid(postDto)) {
			log.error("update", "Post value is null or not valid.");
			throw new ActionFailedException(EntityType.POST.getName(), ActionType.UPDATE.getName());
		}

		validateDto(postDto, ActionType.UPDATE.getName());

		Post post = postRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(EntityType.POST.getName(), "id", id));

		post.setContent(postDto.getContent());
		post.setStatus(postDto.getStatus());
		post.setPrivacy(postDto.getPrivacy());
		post.setUser(userMapper.mapDtoToEntity(new UserDto(postDto.getUserId())));

		Post updatedPost = postRepository.save(post);
		return postMapper.mapEntityToDto(updatedPost);
	}

	@Override
	public void delete(long id) {
		Post post = postRepository.
				findById(id)
				.orElseThrow(
						() -> new EntityNotFoundException(EntityType.POST.getName(), "id", id)
				);
		postRepository.delete(post);
	}

	// TODO: refactor with validation.
	private boolean isUserValid(PostDto postDto) {
		User user = userRepository.findById(postDto.getUserId()).orElseThrow(() -> new EntityNotFoundException(EntityType.POST.getName(), "userId", postDto.getUserId()));
		return user != null;
	}

	// TODO: refactor with validation and exception.
	private void validateDto(PostDto postDto, String action) {
		final String methodName = "validateDto";

		if (postDto.getPrivacy() != null && !PostEnum.isPrivacyValid(postDto.getPrivacy())) {
			log.error(methodName, "Post privacy is not valid.");
			throw new ActionFailedException(EntityType.POST.getName(), action);
		}

		if (postDto.getStatus() != null && !PostEnum.isStatusValid(postDto.getStatus())) {
			log.error(methodName, "Post status is not valid.");
			throw new ActionFailedException(EntityType.POST.getName(), action);
		}
	}
}
