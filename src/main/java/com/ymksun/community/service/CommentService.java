package com.ymksun.community.service;

import java.util.List;

import com.ymksun.community.dto.CommentDto;

public interface CommentService {

	List<CommentDto> getList(String filter);

	CommentDto getById(long id);

	CommentDto save(CommentDto commentDto);

	CommentDto update(long id, CommentDto commentDto);

	void delete(long id);

}
