package com.ymksun.community.service;

import java.util.List;

import com.ymksun.community.dto.PostDto;

public interface PostService {

	List<PostDto> getList(String filter);

	PostDto getById(long id);

	PostDto save(PostDto postDto);

	PostDto update(long id, PostDto postDto);

	void delete(long id);

}
