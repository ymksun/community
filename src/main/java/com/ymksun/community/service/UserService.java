package com.ymksun.community.service;

import java.util.List;

import com.ymksun.community.dto.UserDto;

public interface UserService {

	List<UserDto> getList(String filter);

	UserDto getById(long id);

	UserDto save(UserDto userDto);

	UserDto update(long id, UserDto userDto);

	void delete(long id);

}
