package com.ymksun.community;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ymksun.community.util.log.ApplicationLogger;

@SpringBootApplication
public class CommunityApplication {

	private static final ApplicationLogger log = new ApplicationLogger(CommunityApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CommunityApplication.class, args);
		log.info("Application Started...");
	}

}
