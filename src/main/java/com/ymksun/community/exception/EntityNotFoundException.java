package com.ymksun.community.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

	public EntityNotFoundException(String entityName, String fieldName, Object fieldValue) {
		super(String.format("%s not found with %s : '%s'", entityName, fieldName, fieldValue));
	}
}
