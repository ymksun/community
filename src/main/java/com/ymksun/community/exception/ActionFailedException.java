package com.ymksun.community.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ActionFailedException extends RuntimeException {

	public ActionFailedException(String entityName, String actionName) {
		super(String.format("%s %s action failed!", entityName, actionName));
	}
}
