package com.ymksun.community.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ymksun.community.dto.UserDto;
import com.ymksun.community.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping
	public ResponseEntity<List<UserDto>> getUserList(@RequestParam("queryFilter") String filter) {
		return new ResponseEntity<List<UserDto>>(userService.getList(filter), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<UserDto> getUser(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<UserDto> createUser(@Validated @RequestBody UserDto user) {
		return new ResponseEntity<>(userService.save(user), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<UserDto> updateUser(@PathVariable(value = "id") Long id,
			@Validated @RequestBody UserDto user) {
		return new ResponseEntity<>(userService.update(id, user), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long id) {
		userService.delete(id);
		return ResponseEntity.ok().build();
	}
}
