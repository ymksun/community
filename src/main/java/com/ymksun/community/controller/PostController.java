package com.ymksun.community.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ymksun.community.dto.PostDto;
import com.ymksun.community.service.PostService;

@RestController
@RequestMapping("/api/post")
public class PostController {

	@Autowired
	PostService postService;

	@GetMapping
	public ResponseEntity<List<PostDto>> getPostList(@RequestParam("queryFilter") String filter) {
		return new ResponseEntity<List<PostDto>>(postService.getList(filter), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<PostDto> getPost(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<>(postService.getById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<PostDto> createPost(@Validated @RequestBody PostDto post) {
		return new ResponseEntity<>(postService.save(post), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<PostDto> updatePost(@PathVariable(value = "id") Long id,
			@Validated @RequestBody PostDto post) {
		return new ResponseEntity<>(postService.update(id, post), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deletePost(@PathVariable(value = "id") Long id) {
		postService.delete(id);
		return ResponseEntity.ok().build();
	}

}
