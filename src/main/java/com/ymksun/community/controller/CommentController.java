package com.ymksun.community.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ymksun.community.dto.CommentDto;
import com.ymksun.community.service.CommentService;

@RestController
@RequestMapping("/api/comment")
public class CommentController {

	@Autowired
	CommentService commentService;
	@GetMapping
	public ResponseEntity<List<CommentDto>> getCommentList(@RequestParam("queryFilter") String filter) {
		return new ResponseEntity<List<CommentDto>>(commentService.getList(filter), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<CommentDto> getComment(@PathVariable(value = "id") Long id) {
		return new ResponseEntity<>(commentService.getById(id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<CommentDto> createComment(@Validated @RequestBody CommentDto comment) {
		return new ResponseEntity<>(commentService.save(comment), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CommentDto> updateComment(@PathVariable(value = "id") Long id,
			@Validated @RequestBody CommentDto comment) {
		return new ResponseEntity<>(commentService.update(id, comment), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteComment(@PathVariable(value = "id") Long id) {
		commentService.delete(id);
		return ResponseEntity.ok().build();
	}

}
