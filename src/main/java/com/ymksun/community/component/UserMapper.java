package com.ymksun.community.component;

import com.ymksun.community.dto.UserDto;
import com.ymksun.community.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserMapper {

    public User mapDtoToEntity(UserDto dto) {
        User entity = new User();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
        entity.setEmail(dto.getEmail());
        return entity;
    }

    public UserDto mapEntityToDto(User entity) {
        UserDto dto = new UserDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setEmail(entity.getEmail());
        return dto;
    }

    public List<UserDto> mapEntityToDtoList(List<User> entityList) {
        List<UserDto> dtoList = new ArrayList<>();
        entityList.stream()
                .forEach(entity -> dtoList.add(mapEntityToDto(entity)));
        return dtoList;
    }
}
