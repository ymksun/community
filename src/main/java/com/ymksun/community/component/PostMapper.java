package com.ymksun.community.component;

import com.ymksun.community.dto.PostDto;
import com.ymksun.community.dto.UserDto;
import com.ymksun.community.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PostMapper {

    @Autowired
    UserMapper userMapper;

    public Post mapDtoToEntity(PostDto dto) {
        Post entity = new Post();
        entity.setId(dto.getId());
        entity.setContent(dto.getContent());
        entity.setPrivacy(dto.getPrivacy());
        entity.setStatus(dto.getStatus());
        entity.setUser(userMapper.mapDtoToEntity(new UserDto(dto.getId())));
        return entity;
    }

    public PostDto mapEntityToDto(Post entity) {
        PostDto dto = new PostDto();
        dto.setId(entity.getId());
        dto.setContent(entity.getContent());
        dto.setPrivacy(entity.getPrivacy());
        dto.setStatus(entity.getStatus());
        dto.setUserId(entity.getUser().getId());
        return dto;
    }

    public List<PostDto> mapEntityToDtoList(List<Post> entityList) {
        List<PostDto> dtoList = new ArrayList<>();
        entityList.stream()
                .forEach(entity -> dtoList.add(mapEntityToDto(entity)));
        return dtoList;
    }
}
