package com.ymksun.community.component;

import com.ymksun.community.dto.CommentDto;
import com.ymksun.community.dto.PostDto;
import com.ymksun.community.dto.UserDto;
import com.ymksun.community.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CommentMapper {

    @Autowired
    UserMapper userMapper;

    @Autowired
    PostMapper postMapper;

    public Comment mapDtoToEntity(CommentDto dto) {
        Comment entity = new Comment();
        entity.setId(dto.getId());
        entity.setContent(dto.getContent());
        entity.setUser(userMapper.mapDtoToEntity(new UserDto(dto.getUserId())));
        entity.setPost(postMapper.mapDtoToEntity(new PostDto(dto.getPostId())));
        return entity;
    }

    public CommentDto mapEntityToDto(Comment entity) {
        CommentDto dto = new CommentDto();
        dto.setId(entity.getId());
        dto.setContent(entity.getContent());
        dto.setUserId(entity.getUser().getId());
        dto.setPostId(entity.getPost().getId());
        return dto;
    }

    public List<CommentDto> mapEntityToDtoList(List<Comment> entityList) {
        List<CommentDto> dtoList = new ArrayList<>();
        entityList.stream()
                .forEach(entity -> dtoList.add(mapEntityToDto(entity)));
        return dtoList;
    }
}
