package com.ymksun.community.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", length = 300, nullable = false)
	private String name;

	@Column(name = "email", length = 300, nullable = false)
	private String email;

	@Column(name = "address", length = 300, nullable = false)
	private String address;

}
