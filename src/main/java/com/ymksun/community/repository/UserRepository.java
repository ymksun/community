package com.ymksun.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ymksun.community.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("SELECT u FROM User u WHERE CONCAT(u.id, u.name, u.email, u.address) LIKE %?1%")
	public List<User> search(String keyword);

}
