package com.ymksun.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ymksun.community.model.Comment;

@Repository
public interface CommentRepository  extends JpaRepository<Comment, Long> {

	@Query("SELECT c FROM Comment c WHERE CONCAT(c.id, c.content, c.user.id, c.post.id) LIKE %?1%")
	public List<Comment> search(String keyword);

}
