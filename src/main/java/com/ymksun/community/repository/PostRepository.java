package com.ymksun.community.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ymksun.community.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

	@Query("SELECT p FROM Post p WHERE CONCAT(p.id, p.content, p.status, p.privacy, p.user.id) LIKE %?1%")
	public List<Post> search(String keyword);

}
