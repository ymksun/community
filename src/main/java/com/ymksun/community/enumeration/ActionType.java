package com.ymksun.community.enumeration;

public enum ActionType {
	SEARCH ("search"),
	CREATE ("create"),
	UPDATE ("update"),
	DELETE ("delete");

	private String name;

	ActionType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
