package com.ymksun.community.enumeration;

public class PostEnum {

	public enum Status {
		ACTIVE ("active"),
		INACTIVE ("inactive");

		private String value;

		Status(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

	}

	public enum Privacy {
		PRIVATE ("private"),
		PUBLIC ("public");

		private String name;

		Privacy(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}

	public static boolean isStatusValid(String value) {
		for (Status status: Status.values()) {
			if (value.equals(status.getValue())) {
				return true;
			}
		}
		return false;
	}

	public static boolean isPrivacyValid(String value) {
		for (Privacy status: Privacy.values()) {
			if (value.equals(status.getName())) {
				return true;
			}
		}
		return false;
	}
}
