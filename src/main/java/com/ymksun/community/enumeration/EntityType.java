package com.ymksun.community.enumeration;

public enum EntityType {
	USER ("User"),
	POST ("Post"),
	COMMENT ("Comment");

	private String name;

	EntityType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
