# Getting Started

### User Communication Project
* Java 8
* Spring Boot
* JPA

## Model
* User
* Post
* Comment

### User
* id (auto-generated)
* name
* email (format validated)
* address

### Post
* id (auto-generated)
* content
* privacy (nullable,["private","public"])
* status (nullable, ["active","inactive"])
* userId (foreign-key -> User)

### Comment
* id (auto-generated)
* content
* userId (foreign-key -> User)
* postId (foreign-key -> Post)

_**Note:** The sample API calls are added to postman file._





